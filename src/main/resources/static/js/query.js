result_string = "";
type = {
    'fl': 'fl',
    'ul': 'ul',
};
/*---------Util----------------------*/


function createKey(user_input){
    /*var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
        + (currentdate.getMonth()+1)  + "/"
        + currentdate.getFullYear() + " @ "
        + currentdate.getHours() + ":"
        + currentdate.getMinutes() + ":"
        + currentdate.getSeconds();*/
    return "input"+"_"+user_input;
}

function f(result) {
    //var dataObject = eval('[{"COLUMNS":[{ "title": "NAME"}, { "title": "COUNTY"}],"DATA":[["John Doe","Fresno"],["Billy","Fresno"],["Tom","Kern"],["King Smith","Kings"]]}]');
    var columns = [];

    var columnArray = Object.keys(result.headers).map(function (col, index) {
        var columnObject = {};
        columnObject.data = col;
        return columnObject;
    });
    var columnHeader = $('#example');
    var headerColumn = $('<div>');
    columnHeader.empty();
    //$('.table-scrollable').empty();
    var table = $('<table class="table table-striped table-bordered table-hover custom-tableData" id="a">"');
    var thead = $('<thead>');
    var tr = $('<tr>');
    $.each(result.headers, function (index, header) {
        tr.append('<th scope = "col">' + header + '</th>');
        headerColumn.append('<label><input type="checkbox" checked data-column="' + index + '">' + header + '</label>');
    });
    columnHeader.html(headerColumn);
    thead.append(tr);
    table.append(thead);
    $('#example').append(table);
    $('#a').dataTable( {
        "data": result.rows,
        "columns": columnArray,
        "destroy": true
    } );

    /*
    $('#example').dataTable({
        "data": dataObject[0].DATA,
        "columns": dataObject[0].COLUMNS,
        "destroy": true
    });
    */
}

function getLocalStorage(){
    var len = localStorage.length;
    var ul = "";
    var fl = ul;
    var keys = [];
    for ( var i = 0, len = len; i < len; ++i ) {
        var key = localStorage.key( i );
        var val = JSON.parse(localStorage.getItem(key));
        var name = val.name;
        if (key.startsWith("input_") && !val.name) {
            //history - TODO save type in object
            ul = ul + ('<li onclick="execLocalStorage(\'' +key+ '\',\'' +type.ul+ '\')">'+key+'</li>');
        } else if(key.startsWith("input_") && val.name){
            //fav
            fl = fl + ('<li onclick="execLocalStorage(\'' +key+ '\',\'' +type.fl+ '\')">'+name+'</li>');
        }
    }
    if(ul)
        ul = "<ul>" + ul +("</ul>");

    if(fl)
        fl = "<ul>" + fl +("</ul>");

    return pojo('ul', 'fl')(ul, fl);
}

function runQuery(input) {
    if(!input) {
        alert("0 char found");
        return;
    }
    $.ajax(
        {
            url: "/api/create",
            method: 'POST',
            dataType: 'JSON',
            contentType: 'application/json',
            data: input,
            processData:false,
            success: function(result){
                result_string = result;
                $("#error").hide();
                f(result);
                save(input,result);
                $(".save").show();
            },
            error: function(result){
                result_string = "";
                $("#example_wrapper").hide();
                document.getElementById("error").innerHTML = result.responseJSON.message;
                $('#error').addClass("text-danger");
                //save(input,result,false);
            }
        });
}

function save(user_input, result, name){
    // Put the object into storage
    var key = createKey(user_input, JSON.stringify(result));
    var name = name ? name : null;
    var value = historyPOJO(key, JSON.stringify(result), name);
    localStorage.setItem(key, JSON.stringify(value));
    showHistory();
    showFavourites();
}

/*---------Custom----------------------*/
function execLocalStorage(key, t) {
    var result_json = JSON.parse(localStorage.getItem(key));
    $(".query").val(result_json.key.replace("input_",""));
    f(JSON.parse(result_json.value));
    if(t == type.fl) {
        $('.save').hide();
    } else {
        $('.save').show();
    }
}

function  showFavourites() {
    $("#fav_result").empty();
    var input = getLocalStorage().fl;
    if(!input) {
        alert("0 Items");
        return;
    }
    $("#fav_result").append(input);

}

function showHistory() {
    $("#history_result").empty();
    var input = getLocalStorage().ul;
    if(!input) {
        alert("0 Items");
        return;
    }
    $("#history_result").append(input);
}

function saveFav(){
    if ( $('#example').text().length == 0 ) {
        alert("query cannot be saved");
        return;
    }
    save($(".query").val(), result_string, prompt('save this query as...'));
}

function clearHistory(){
    if(confirm("clear history")){
        localStorage.clear();
        showHistory();
        showFavourites();
    }
}
/*-----------Core--------------------*/

function historyPOJO(key, value, name){
    return pojo('key', 'value', 'name')(key, value, name);
}

var pojo = function () {
    var members = arguments;

    return function () {
        var obj = {}, i = 0, j = members.length;
        for (; i < j; ++i) {
            obj[members[i]] = arguments[i];
        }

        return obj;
    };
};

