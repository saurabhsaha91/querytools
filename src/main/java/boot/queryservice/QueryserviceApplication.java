package boot.queryservice;

import boot.queryservice.model.FileUpload;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
        FileUpload.class
})
public class QueryserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(QueryserviceApplication.class, args);
    }
}
