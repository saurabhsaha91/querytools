package boot.queryservice.utils;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

import java.io.IOException;

public class JSONUtility {
    static ObjectMapper om = new ObjectMapper();

    public static String JSONtoString(Object object) throws IOException {
        om.setVisibility(JsonMethod.FIELD, JsonAutoDetect.Visibility.ANY);
        return om.writeValueAsString(object);
    }

}
