package boot.queryservice.repository;

import boot.queryservice.model.QueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class PersonRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public QueryResponse runQuery(String sql){
        QueryResponse response = new QueryResponse();

        List<String> headers = new ArrayList<>();
        List<List<Object>> rows = new ArrayList<>();

        List<Map<String, Object>> queryForList = jdbcTemplate.queryForList(sql);
        if(headers.isEmpty()) {
            headers.addAll(queryForList.get(0).keySet());
        }

        for(Map<String, Object> map : queryForList) {
            List<Object> row = new ArrayList<>();
            for(String k : map.keySet()) {
                row.add(map.get(k));
            }
            rows.add(row);
        }

        response.setHeaders(headers);
        response.setRows(rows);

        return response;
    }
}


