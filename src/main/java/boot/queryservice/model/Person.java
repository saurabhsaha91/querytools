package boot.queryservice.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Person implements Serializable {
    String name;
    int age;
    long id;
}
