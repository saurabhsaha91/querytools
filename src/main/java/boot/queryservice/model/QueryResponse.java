package boot.queryservice.model;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class QueryResponse implements Serializable {
    List<String> headers = new ArrayList<>();
    List<List<Object>> rows = new ArrayList<>();

    public QueryResponse() {
    }

    public void setHeaders(List<String> headers) {
        this.headers = headers;
    }

    public void setRows(List<List<Object>> rows) {
        this.rows = rows;
    }

}
