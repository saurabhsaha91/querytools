<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html lang="en" class="no-js">
<head>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <script type="text/javascript" src="js/query.js"></script>
    <link href="css/query.css" rel="stylesheet">
</head>
<body>

<div class="container-fluid">
    <div>
        <button class="float-right" onclick="clearHistory()">Clear History</button>
        <div>
            <input type="file" name="files"/>
            <button type="button" onclick="load()">Upload DB CSV</button>
        </div>
    </div>

    <hr>

    <div class="row row-divided">
        <!--Menu-->
        <div class="col-xs-2 column-one">
            <div>
                <button type="history" onclick="showHistory()">History</button>
                <div id="history_result"></div>
            </div>
            <div>
                <button type="history" onclick="showFavourites()">Favourite</button>
                <div id="fav_result"></div>
            </div>

        </div>
        <div class="vertical-divider"></div>
        <!--Input-->
        <div class="col-xs-10 column-two">
            <div>
                <textarea class="query" rows="4" cols="50"></textarea>
            </div>
            <div>
                <button type="button" onclick="runQuery($('.query').val())">Run</button>
                <button type="button" onclick="saveFav()" style="display: none" class="save">Save</button>
            </div>

            <div id="example"></div>

            <div id="error">

            </div>
        </div>
    </div>
</div>

    <script>
        $(document).ready(function(){
        });
    </script>
</body>
</html>

