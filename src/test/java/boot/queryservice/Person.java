package boot.queryservice;

import java.util.ArrayList;
import java.util.List;

public class Person {
    int age;
    String name;

    String speak() {
        return "papa";
    }

    public static void main(String[] args){
        Person p = new Person();
        Person m = new Male();
        List<Person> list = new ArrayList<>();
        list.add(p);
        list.add(m);

        for(Person person : list){
            System.out.print(person.speak());
        }
    }
}
