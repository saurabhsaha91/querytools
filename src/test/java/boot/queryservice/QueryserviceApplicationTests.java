package boot.queryservice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class QueryserviceApplicationTests {

    public static void main(String[] args){
        Person p = new Person();
        Person m = new Male();
        List<Person> list = new ArrayList<>();
        list.add(p);
        list.add(m);

        for(Person person : list){
            System.out.print(person.speak());
        }
    }
    @Test
    public void contextLoads() {
    }

}
